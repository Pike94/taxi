'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}

$(document).on('click', '.menu-btn', function() {
  $('.menu-overaly').fadeIn();
  $('.menu').addClass('active');
});

$(document).on('click', '.menu-overaly', function() {
  $('.menu-overaly').fadeOut();
  $('.menu').removeClass('active');
  $('.page').fadeOut();
});

$(document).on('click', '.close-menu', function() {
  $('.menu-overaly').fadeOut();
  $('.menu').removeClass('active');
  $('.page').fadeOut();
});

$(document).on('click', '.menu-list-btn', function() {
  let target = $(this).data('section-target');

  $('.menu-list-btn').removeClass('active');
  $(this).addClass('active')
  if(target === 'modal') {
      var modal_btn = $('.btn[data-remodal-target="form-modal"]');
      modal_btn.trigger('click');
    $('.page').fadeOut();
    $('.menu-overaly').fadeOut();
    //setTimeout(function() { $('#' + target).fadeIn(); }, 400);
    $('.menu').removeClass('active');
  } else{
    $('.page').fadeOut();
    setTimeout(function() {
      $('#' + target).fadeIn();
    }, 400)
  }
});

$(document).on('click', '.close-page', function() {
  $('.page').fadeOut();
});

$(document).on('click', '.close-page-modal', function() {
  $('.page').fadeOut();
  $('.menu').addClass('active');
  $('.menu-list-btn').removeClass('active');
});

$(document).on('submit', '.modal-form', function(e) {
  e.preventDefault();
  var form = $(this);
  $.ajax({
      type: "POST",
      url: '/mail.php',
      cache: false,
      data: form.serialize(),
      dataType: 'json',
      timeout: 3000,
      beforeSend: function () {
          form.find('input,textare').removeClass('error');
          form.find('.input-error').detach();
      },
      success: function (response) {
          if (response.success) {
              form.addClass('success').html(response.success)
          } else {
              var errors = response.errors;
              for (var key in errors ) {
                  form.find("input[name*='"+key+"']").addClass('error').parent().append('<div class="input-error">'+errors[key]+'</div>');
              }
          }
      },
      error: function (jqXHR, exception) {
          var msg = '';
          if (jqXHR.status === 0) {
              msg = 'Not connect.\n Verify Network.';
          } else if (jqXHR.status == 404) {
              msg = 'Requested page not found. [404]';
          } else if (jqXHR.status == 500) {
              msg = 'Internal Server Error [500].';
          } else if (exception === 'parsererror') {
              msg = 'Requested JSON parse failed.';
          } else if (exception === 'timeout') {
              msg = 'Time out error.';
          } else if (exception === 'abort') {
              msg = 'Ajax request aborted.';
          } else {
              msg = 'Uncaught Error.\n' + jqXHR.responseText;
          }
           console.log(msg);
      }
  });
});